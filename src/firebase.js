import firebase from 'firebase'
const config = {
  apiKey: "AIzaSyB15lQIoT5bgq3EUM3D376_6MjmZdoTtt8",
  authDomain: "polling-18e3b.firebaseapp.com",
  databaseURL: "https://polling-18e3b.firebaseio.com",
  projectId: "polling-18e3b",
  storageBucket: "",
  messagingSenderId: "596787693984",
  appId: "1:596787693984:web:76bb3ce8f1946c25"
};
firebase.initializeApp(config);
export const provider = new firebase.auth.GoogleAuthProvider();
export const auth = firebase.auth();
export default firebase;