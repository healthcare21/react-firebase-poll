import  { auth } from '../firebase.js';

export 	const logout = (setState) => {
    auth.signOut()
    .then(() => {
      setState({
        user: null
      });
    
    });
    window.location.href = './';

};



