import firebase from '../firebase.js';


export 	const populateCurrent = (setState) => {
    let currentState = firebase.firestore().collection('currentState');
    currentState.onSnapshot(function(doc) {
         setState({
            currentState: doc.docs[0].data().state,
            currentStateID: doc.docs[0].id
         });
    });
};

export 	const getCurrentspeaker = (setState) => {
    let currentState = firebase.firestore().collection('currentSpeaker');
    currentState.onSnapshot(function(doc) {
         setState({
            currentSpeaker: doc.docs[0].data().Name,
            currentSpeakerID: doc.docs[0].id
         });
    });
};

export 	const allowQuestions = (setState) => {
    let currentState = firebase.firestore().collection('allowQuestion'); 
    currentState.onSnapshot(function(doc) {
         setState({
            allowQuestion: doc.docs[0].data().allowquestions
         });
    });
};

export const getAllquestions = (setState) => {
    let currentState = firebase.firestore().collection('Questions'); 
    currentState.onSnapshot(function(doc) { 
        let returnArrs = [];
        doc.docs.map((doc) => {
            returnArrs.push({'Title':doc.data().Text, 'id':doc.id, 'IsModerated': doc.data().moderated, 'pollId': doc.data().pollID});
        })
         setState({
            questions: returnArrs
         });
    });
};

export const getAllspeakers = (setState) => {
    let currentState = firebase.firestore().collection('speakers'); 
    currentState.get().then(function(doc) { //Don't need onSnapshot as wquestions will be already set
        let returnArrs = [];
        doc.docs.map((doc) => {
            returnArrs.push({'Name':doc.data().Name, 'id':doc.id, 'Description': doc.data().Description, 'Image': doc.data().Image});
        })
         setState({
            allSpeakers: returnArrs
         });
    });
};

export 	const setQuestionModerated = (setState) => {
    let currentState = firebase.firestore().collection('allowQuestion'); 
    currentState.onSnapshot(function(doc) {
         setState({
            allowQuestion: doc.docs[0].data().allowquestions
         });
    });
};


export 	const populateQuestion = (setState) => {
    let questionRef = firebase.firestore().collection('questions');
    questionRef.onSnapshot(function(snapshot) {
        // console.log("Current data: ", doc.docs[0].data().state);
        const docs = snapshot.docs.map((docSnapshot) => ({
            id: docSnapshot.id,
            data: docSnapshot.data()
          }));
         setState({
            currentQuestions: docs
         });
    });
};
