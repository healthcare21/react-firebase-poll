import React, { Component } from 'react';
import { BrowserRouter, Route, Link } from 'react-router-dom';
import firebase, { auth } from './firebase.js';
import Homepage from './components/homepage'
import Questions from './components/questions'
import Admin from './components/admin'
import Presentation from './components/presentation'
import AddQuestion from './components/addQuestion'
import Agenda from './components/agenda'
import Speakers from './components/speakers'
import AskQuestion from './components/askQuestion'
import {populateCurrent, populateQuestion, allowQuestions, getAllspeakers, getCurrentspeaker} from './utilities/firebase_queries'

import Header from './components/header'
import Footer from './components/footer'

import './components/css/core/_global.scss';
import header from './components/css/header.module.scss'
import login from './components/css/login.module.scss'

class App extends Component {
  constructor() {
    super();

    // this.questionRef = firebase.firestore().collection('questions');
    this.userRef = firebase.firestore().collection('users');
     this.currentState = firebase.firestore().collection('currentState');

    this.state = {
      currentItem: '',
      currentQuestions: '',
      allSpeakers: '',
      currentState: '',
      currentStateID: '',
      username: '',
      items: [],
      userlevel: '',
      catagories: [],
      user: null,
      userValues: [],
      allSpeakers: [],
      code: ''

    }
    this.handleChange = this.handleChange.bind(this);
    this.setProps = this.setProps.bind(this);
    this.login = this.login.bind(this); 

  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  populateUserFire = (snapshot) => {
    const docs = snapshot.docs.map((docSnapshot) => ({
      id: docSnapshot.id,
      data: docSnapshot.data(),
    }));
    if(docs[0]){
      localStorage.setItem('username', docs[0].data.name);
      localStorage.setItem('useremail', docs[0].data.email);
    }
  };


  login(e, username, password) {
    e.preventDefault();
    let passwordr; let usernamer; let redirect = false;
    if(this.state.code == '445566'){
      passwordr = 'Mrbarbie9';
      usernamer = 'webape2@gmail.com'
    } else if (this.state.code == '778899') {
      passwordr = 'Mrbarbie9';
      usernamer = 'matt.burton@healthcare21.co.uk';
      redirect = true;
    } else {
      alert('Sorry there was an issue with the ode - please try again.')
    }
    
    auth.signInWithEmailAndPassword(usernamer,passwordr) 
      .then((result) => {
        const user = result.user;
        populateCurrent(this.setState.bind(this)); //on login make sure we have the latest current state
        populateQuestion(this.setState.bind(this));//on login make sure we have the latest questions from firebase
        allowQuestions(this.setState.bind(this));//on login are questions allowed
        // getAllspeakers(this.setState.bind(this));//on login get all speakers
        getCurrentspeaker(this.setState.bind(this));//on login get current Speaker
        if(redirect){
          window.location.href = './admin';
        }
      }) .catch(err => {
        alert(err.message);
      });
  }

  componentDidMount() {
    if(auth){
      populateCurrent(this.setState.bind(this)); //get the latest current state - used to navigate questions
      populateQuestion(this.setState.bind(this));//get the latest questions from firebase
      allowQuestions(this.setState.bind(this));//are questions currently allowed
      // getAllspeakers(this.setState.bind(this));// get all speakers
      getCurrentspeaker(this.setState.bind(this));// get current Speaker
    }
    auth.onAuthStateChanged((user) => {
      if (user) {
        this.setState({ user });
        this.userRef.where("email", "==", this.state.username).onSnapshot(this.populateUserFire);
        if(user.email.includes("healthcare21")){       //Get all questions to pass to props.
          this.setState({
              userlevel: 'admin'
            })
        }
      } 
    });
  }


  setProps(newValue){
      this.setState({
        currentState: newValue
      })
      //Trigger database change.
      this.currentState.doc(this.state.currentStateID).set({state: newValue})
  }

  render() {
    // console.log('this.state',this.state);
    let link;
      link = <><ul class="menu"><li><Link to="/">Home</Link></li><li><Link to="/myKameo">Questions </Link></li></ul></>
      return (
      <div className='app'>
        {this.state.user ?
          <BrowserRouter >
              {/* <Menu right>
                {link}
              </Menu> */}
            <Route path="/presentation" exact render={(props)=> <Presentation currentSpeaker={this.state.currentSpeaker} currentQuestions={this.state.currentQuestions} userValues={this.state.userValues} currentState={this.state.currentState} setProps={this.setProps} {...props}  />} />
            <Route path="/" exact render={(props)=> <Homepage  currentSpeaker={this.state.currentSpeaker} userlevel={this.state.userlevel}  {...props}  />} />
            <Route path="/addNew:id" exact render={(props)=> <AddQuestion userlevel={this.state.userlevel} currentQuestions={this.state.currentQuestions} userValues={this.state.userValues} currentState={this.state.currentState} {...props}  />} />
            <Route path="/admin" exact render={(props)=> <Admin currentSpeaker={this.state.currentSpeaker} userlevel={this.state.userlevel}  allowQuestion={this.state.allowQuestion} currentQuestions={this.state.currentQuestions} userValues={this.state.userValues} currentState={this.state.currentState} setProps={this.setProps} {...props}  />} />
            <Route path="/questions" exact render={(props)=> <Questions currentSpeaker={this.state.currentSpeaker} allSpeakers={this.state.allSpeakers} userlevel={this.state.userlevel}  allowQuestion={this.state.allowQuestion} currentQuestions={this.state.currentQuestions} userValues={this.state.userValues} currentState={this.state.currentState} setProps={this.setProps} {...props}  />} />
            <Route path="/agenda" exact render={(props)=> <Agenda currentSpeaker={this.state.currentSpeaker} userlevel={this.state.userlevel}  {...props}  />} />
            <Route path="/speakers" exact render={(props)=> <Speakers currentSpeaker={this.state.currentSpeaker} userlevel={this.state.userlevel}  {...props}  />} />
            <Route path="/askquestion" exact render={(props)=> <AskQuestion currentSpeaker={this.state.currentSpeaker} userlevel={this.state.userlevel}  allowQuestion={this.state.allowQuestion} {...props}  />} />
         
          </BrowserRouter>
            :
          <>
          <div className="container-fluid flex">
            <div className="g--3 g--sm-12 noPad">
				<header>
					<div clss="container">
						<div class="g--12">
							<h1>Welcome to HC21 event Macclesfield</h1>
						</div>
					</div>
				</header>
			</div>
            {this.state.user ?
				<button onClick={this.logout}>Log Out</button>                
			:
				<div className="g--9 g--sm-12">
					<div className={login.formContainer}>
						<form onSubmit={this.login} className="loginForm">
							<h3>Login</h3>
							<input type="text" name="code" placeholder="Code" onChange={this.handleChange}/>
							{/* <input type="text" name="username" placeholder="Username" onChange={this.handleChange}/>
							<input type="password" name="password" placeholder="Password?" onChange={this.handleChange}/> */}
							<button>Login</button>
						</form>
					</div>
				</div>
			}
			</div>
          </>
      }
      <Footer />
      </div>
    );
  }
}
export default App;