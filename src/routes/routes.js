import Homepage from './components/homepage'
import Categories from './components/categories'
import Edititems from './components/editItems'
import Editsingle from './components/editsingle'
import Secondary from './components/secondaryPage'
import Tertiary from './components/tertiary'

const Routes = (
    <BrowserRouter >
        <Route path="/addcat" exact render={()=> <Categories user={this.state.user} cats={this.state.catagories} />} />
        <Route path="/" exact render={()=> <Homepage cats={this.state.catagories} />} />
        <Route path="/additems" exact render={()=> <Edititems user={this.state.user} items={this.state.items}  cats={this.state.catagories} />} />
        <Route path="/edititem:id" exact render={(props)=> <Editsingle user={this.state.user} items={this.state.items}  cats={this.state.catagories} {...props}  />} />
        <Route path="/secondary:id" exact render={(props)=> <Secondary items={this.state.items}  cats={this.state.catagories} {...props}  />} />
        <Route path="/tertiary:id" exact render={(props)=> <Tertiary items={this.state.items}  cats={this.state.catagories} {...props}  />} /> 
    </BrowserRouter >
  )
   
  export default Routes