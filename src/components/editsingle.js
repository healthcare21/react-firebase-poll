import React, { Component } from 'react';
import firebase, { } from '../firebase.js';

class Editsingle extends Component {

  constructor(props) {
    super(props);
    this.projectRef = firebase.firestore().collection('project');
    this.state = {
        mainCategory: '',
        secondaryLevelboo: false,
        secondaryCategory: '',
        subtitle: '',
        username: '',
        mainDescription: '',
        items: this.props.items,
        cats: this.props.cats,
        resources: [{name: '',bucketType: '', description: '', assets: [{title: '', link: '', type: '', description: ''}]}]
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeCheckbox = this.handleChangeCheckbox.bind(this);
  }

componentDidMount(){
   let currentId = this.props.match.params.id;
   let foundItem = false;
    {this.props.items.map((item) => {
             if (item.id === currentId){
              foundItem = true;
               this.setState(
                 {
                cats: this.props.cats,
                mainCategory: item.data.mainCategory,
                secondaryLevelboo: item.data.secondaryLevelboo,
                secondaryCategory: item.data.secondaryCategory,
                 mainDescription : item.data.mainDescription,
                subtitle: item.data.subtitle,
                username: item.data.username,
                items: item.data.items,
                resources: item.data.resources
                 }
               )
         }
    })
    }
    if(!foundItem){
      alert('Something went wrong!!!');
      this.props.history.push({pathname: '/'})
    }

}

  handleChangeCheckbox(e) {
    this.setState({
        [e.target.name]: e.target.checked
    });
  }
  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }
  handleChangeResource(index, e)
 {
    const newResources = this.state.resources.slice() //Copies arrary into new array to manipulate
    newResources[index][e.target.name] = e.target.value;
    this.setState({
      resources: newResources
    });

  };

  handleChangeAsset(index, indexassett, e){
    const newResources = this.state.resources.slice() //Copies arrary into new array to manipulate
    newResources[index].assets[indexassett][e.target.name] = e.target.value;
    this.setState({
      resources: newResources
    });
  }


  handleSubmit(e) {
    e.preventDefault();
    if (!this.state.mainCategory) {
        alert ('Please enter a title');
    } else {
            this.projectRef.doc(this.props.match.params.id).update(
              
              {
                mainCategory: this.state.mainCategory,
                secondaryLevelboo: this.state.secondaryLevelboo,
                secondaryCategory: this.state.secondaryCategory,
                mainDescription: this.state.mainDescription,
                resources: this.state.resources
              }
            );  
            alert('Item updated');
            this.props.history.push({pathname: '/'})
    }
  }

  removeItem(itemId) {
    this.projectRef.doc(itemId).delete();
  }

  appendInput() {
    var newInput = {name: '',bucketType: '', assets: [{title: '', link: '', type: ''}]}
    this.setState(prevState => ({ resources: prevState.resources.concat([newInput]) }));
  }

  appendInputR(index, indexr) {
     var newInput = {title: '', link: '', type: ''}
     var concat = this.state.resources[index].assets.concat(newInput);
     this.setState(prevState => ({ assets: prevState.resources[index].assets = concat }));
  }

  render() {
    return (
        <div>
            <h2>Add item</h2>
            <select onChange={this.handleChange} value={this.state.mainCategory} name="mainCategory" >
            <option value = "Please">Please choose</option>
            {this.state.cats.map((item) => {
                     return (
                        <option key={item.id} value={item.data.title}> {item.data.title}  </option>
                  )
             })
            }
            </select>
             <form onSubmit={this.handleSubmit}>
                <input readOnly type="hidden" name="username" placeholder="What's your name?" value={this.props.user.displayName || this.props.user.email} />   
                <input type="checkbox" name="secondaryLevelboo"onChange={this.handleChangeCheckbox} value={this.state.secondaryLevelboo} /> Is this item imediatley under the main catagory?<br/>
                <input type="text" name="secondaryCategory" placeholder="Secondary Catagory" onChange={this.handleChange} value={this.state.secondaryCategory} /> 
                <textarea rows="4" cols="50" name='mainDescription' placeholder="Description" onChange={this.handleChange} value={this.state.mainDescription} /> 
                       
                
                <h5>Resources</h5>
                <div id="dynamicInput">
                       {this.state.resources.map((input,index) => {
                         return(
                           <div>
                          <input type="text" name='name' placeholder="Resource Name" onChange={(e) => this.handleChangeResource(index, e)} value={this.state.resources[index].name} /> 
                          <input type="text" name='bucketType' placeholder="Bucket Type" onChange={(e) => this.handleChangeResource(index, e)} value={this.state.resources[index].bucketType} /> 
                          <textarea rows="4" cols="50" name='description' placeholder="Description" onChange={(e) => this.handleChangeResource(index, e)} value={this.state.resources[index].description} /> 
                         {input.assets.map((inputr,indexr) => {
                              return( 
                              <div>
                                <input type="text" name='title' placeholder="Asset title" onChange={(e) => this.handleChangeAsset(index,indexr, e)} value={this.state.resources[index].assets[indexr].title} /> 
                                <input type="text" name='link' placeholder="Asset link" onChange={(e) => this.handleChangeAsset(index,indexr, e)} value={this.state.resources[index].assets[indexr].link} /> 
                                <input type="text" name='type' placeholder="Asset type (video / image / pdf /url)" onChange={(e) => this.handleChangeAsset(index,indexr, e)} value={this.state.resources[index].assets[indexr].type} />                   
                                <input type="text" name='description' placeholder="Description" onChange={(e) => this.handleChangeAsset(index,indexr, e)} value={this.state.resources[index].assets[indexr].description} />                   
                                <input type="text" name='subtitle' placeholder="Subtitle" onChange={(e) => this.handleChangeAsset(index,indexr, e)} value={this.state.resources[index].assets[indexr].subtitle} />                   
                                

                                <button type="button" onClick={ () => this.appendInputR(index,indexr) }> Add a new asset </button>        
                              </div> 
                              )
                          })}
                          </div>
                          )
                       }
                       )  
                      }
                          <button  type="button" onClick={ () => this.appendInput() }>
                    Add a new resource
               </button>
                </div>

                <button>Submit</button>
            </form>
            <h2>Current items</h2>
            <ul>
            {this.props.items.map((item) => {
                  var editLink = '/edititem/'+item.id;
                     return (
                           <li key= {item.id}> {item.data.mainCategory} | {item.data.secondaryCategory} <a href={editLink}>edit</a> <button onClick={() => this.removeItem(item.id)}>X</button> </li>
                     )
             })
             }
          </ul>
        </div>
    );
  }
}
export default Editsingle;