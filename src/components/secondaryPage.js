import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Secondary extends Component {
  constructor(props) {
    super(props);
    this.state= {
        currentCat : this.props.match.params.id,
        items: []
    }
  }


  componentDidMount() {
    if(this.props.cats.length === 0){
        //No catagories mean the page has been refreshed -- get stored from localstorage
        var lstorage = JSON.parse(localStorage.getItem('secondaryPage'));
        const filterNames = lstorage.filter(obj => obj.data.mainCategory === this.state.currentCat );
        this.setState({
            items: filterNames
        })
      } else {
        localStorage.setItem('secondaryPage', JSON.stringify(this.props.items));
        const filterNames = this.props.items.filter(obj => obj.data.mainCategory === this.state.currentCat );
        this.setState({
            items: filterNames
        })
      }
  }

  render() {
    return (
            <div className="wrappersecondary">
            <ul>
              {this.state.items.map((item) => {
                  return (
                 
                    <li key={item.data.secondaryCategory}>
                       <Link to = {'/tertiary' + item.id}>
                         <h3>{item.data.secondaryCategory}</h3>
                      </Link>
                    </li>

               
                  
                  )
              })} 
                 <li key="myKameo" className="mykameo">
                  <Link to = {'/myKameo'}>
                    <h3>My KAMEO</h3>
                  </Link> 
                  </li>
              </ul>
            </div>
    )
  }
}
export default Secondary;