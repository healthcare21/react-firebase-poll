import React, { Component } from 'react';

import { getAllquestions} from '../utilities/firebase_queries'
import Header from './header'
import { Link } from 'react-router-dom';

import styles from './css/agenda.module.scss';

var _ = require('lodash');

class Agenda extends Component {

  constructor(props) {
  super(props);

  }


  componentDidMount() {
    var TIMEOUT = 2000;
    var lastTime = (new Date()).getTime();
    getAllquestions(this.setState.bind(this)); //get list of all currently asked questions
    setInterval(function() {
      var currentTime = (new Date()).getTime();
      if (currentTime > (lastTime + TIMEOUT + 2000)) {
        // Wake!
        window.location = window.location;
      }
      lastTime = currentTime;
    }, TIMEOUT);
  }





  render() {
    return (
            <div className="homepage">
				<div className="container-fluid flex">
					<div className="g--3 g--sm-12 noPad">
                		<Header header="Agenda" />
					</div>
					<div className="g--9 g--sm-12">

						<div className={styles.agendaList}>
							<div className={styles.item}>
								<div className={styles.time}>
									<p>13:00</p>
								</div>
								<h2 className={styles.title}>Title of presentation, discussion or workshop</h2>
								<small className={styles.presenter}>Name of presenter</small>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ex orci, efficitur non varius vitae, ornare lacinia odio. Donec tempus mauris vel tellus ornare</p>
							</div>

							<div className={styles.item}>
								<div className={styles.time}>
									<p>14:00</p>
								</div>
								<h2 className={styles.title}>Title of presentation 2, discussion or workshop</h2>
								<small className={styles.presenter}>Name of presenter</small>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ex orci, efficitur non varius vitae, ornare lacinia odio. Donec tempus mauris vel tellus ornare</p>
							</div>
						</div>

					</div>
                </div>
            </div>
    )
  }
}
export default Agenda;