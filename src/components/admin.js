import React, { Component } from 'react';
import firebase  from '../firebase.js';
import { getAllquestions, getAllspeakers} from '../utilities/firebase_queries'
import DangerButton from './qmoderated'
import CurSpeaker from './currentspeaker'
import Header from './header'
import { allSettled } from 'q';

import styles from './css/admin.module.scss'

var _ = require('lodash');

class Details extends Component {

  constructor(props) {
  super(props);
   this.state = {
      currentItem: '',
      username: '',
      items: [],
      user: null,
      locked: false,
      currentQuestion: '',
      barGraphdata: [],
      question: '',
      allQuestions: []
    }
    this.questionRef = firebase.firestore().collection('questions');
    this.userQuestions = firebase.firestore().collection('Questions');
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  componentDidMount() {
    var TIMEOUT = 2000;
    var lastTime = (new Date()).getTime();
    getAllquestions(this.setState.bind(this)); //get list of all currently asked questions
    getAllspeakers(this.setState.bind(this));// get all speakers
    setInterval(function() {
      var currentTime = (new Date()).getTime();
      if (currentTime > (lastTime + TIMEOUT + 2000)) {
        // Wake!
        window.location = window.location;
      }
      lastTime = currentTime;
    }, TIMEOUT);
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  handleSubmit(e) {
    e.preventDefault();
    //add to userQuestions
    this.userQuestions.add({
      Text: this.state.question,
      moderated: false,
      pollID: this.props.currentState
    })
    alert('Question posed');
  }

  getQuestion(questionId) {
    let newarr;
    if(this.props.currentState !== 'notStarter') {
        //filterquestions by id
      newarr =   _.filter(this.props.currentQuestions, { 'id': this.props.currentState });
    }
    this.state.currentItem = newarr;
    return newarr;
  }

  onItemClick = (item) => {
    let temporaryarray = this.state.currentItem[0].data;
    temporaryarray.questionsArray[item].Number = temporaryarray.questionsArray[item].Number +1;
    this.setState({
      locked:true
    })
    //Find item in database and add 1
     this.questionRef.doc(this.state.currentItem[0].id).update({
      questionsArray: temporaryarray.questionsArray
     })
     this.setState({
       locked: true
     })
  }


  greeting = () => {
    if(this.state.locked){
      return(
      <h3 className="warning">Thank you for your submission</h3>
      )
    }
  }

  createList = (questions) => {
    let content;
    if(questions.questions){
        content = questions.questions.map((catName, i) => {
              return (
                <div key={i} className={styles.answer}>
                	<p>{catName.Answer}</p><p className={styles.votes}><span>{catName.Number}</span> votes</p>
                </div>
              )
            });
          }
     return content;
  }

createPolls = () => {
    let content;
    if(this.props.currentQuestions){
		const currentQ = this.getQuestion()
        content = this.props.currentQuestions.map((catName, i) => {
			let active = false;
			if( currentQ[0] && currentQ[0].id === catName.id ){
				active = true;
			}
			console.log(currentQ, catName.id)
			return (
				<li className={[styles.pollText, (active ? styles.active : '')].join(' ')} onClick={() => {this.props.setProps(catName.id)}}>
					<p>{catName.data.Title}</p>
				</li>
			)
        });
    }
    return content;
}





  render() {
    if(this.props.userlevel != 'admin'){
      window.location.href = './';
    }
    let title; let description; let questions; let question; let curID;
    if(!this.props.currentState || this.props.currentState === 'notStarted'){      //display welcome message - not started!
      title = "The poll has not been started yet, please wait - this screen will automatically refresh when the poll is open";
      description = "Instructions for the poll"
    } else {
      let currentQ = this.getQuestion();       //get question details from props
      title = currentQ.data;
      if(currentQ[0]) {
        title = currentQ[0].data.Title;
        curID = currentQ[0].id;
        description = currentQ[0].data.Descriptions;
        questions = currentQ[0].data.questionsArray;
      }
    }
   
    return (
        <div className="homepage">
			<div className="container-fluid flex">
				<div className="g--3 g--sm-12 noPad">
          			<Header currentSpeaker={this.props.currentSpeaker} userlevel={this.props.userlevel} />
				</div>
				<div className="g--9 g--sm-12">
					<div className={['card', styles.adminCard, styles.evenPad].join(' ')}>
						<DangerButton questions={this.state.questions} currentID={curID} />
						<div className="flex">
							<div className="g--5 g--md-12">
								<div className={styles.sidebar}>
									<div className={styles.questionNavigation}>
										<h2 className="questionTitle">Navigate questions</h2>
										<ul>
											{this.createPolls()}
										</ul>
										<p className={['button', styles.pollButton, styles.closePoll].join(' ')} onClick={() => {this.props.setProps('notStarted')}}>Close poll</p> 
									</div>
									{this.props.userlevel === 'admin' &&
										<div className="adminButtons">
											<CurSpeaker speakers={this.state.allSpeakers} currentSpeaker={this.props.currentSpeaker} />
										</div>
									}
								</div>
							</div>
							<div className="g--7 g--md-12">
								<div className={styles.currentPoll}>
									<h2>Current Poll</h2>
									<div className={styles.questionInfo}>
										<p className={styles.questionTitle}>{title}</p>
										<p>{description}</p>
									</div>
									<div className={[styles.answerList].join(' ')}>
										{this.createList({questions})}
										{question}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

              {/* <div className="fifty currentPoll"> */}
					{/* {this.greeting()} 
					<h2>Current Poll:</h2>
					<h3 className="questionTitle">{title}</h3>
					<p>{description}</p> */}
					{/* <div className="ulSubstitute">
						{this.createList({questions})}
						{question}
					</div> */}
              {/* </div> */}
        
              {/* <div className="fifty adminQuestions_nav"> */}
              {/* <h2 className="questionTitle">Navigate questions</h2>
                {this.createPolls()}
                  <p onClick={() => {this.props.setProps('notStarted')}}>Close Poll</p> 
              </div>
                {this.props.userlevel === 'admin' &&
                  <div className = "adminButtons">
                    <DangerButton questions={this.state.questions} currentID ={curID} />
                    <CurSpeaker speakers = {this.state.allSpeakers} currentSpeaker={this.props.currentSpeaker} />
                  </div>
                } */}
               
            </div>
    )
  }
}
export default Details;