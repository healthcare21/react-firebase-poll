import React, { Component } from 'react';

import { getAllquestions} from '../utilities/firebase_queries'
import Header from './header'
import { Link } from 'react-router-dom';

import styles from './css/homepage.module.scss';

var _ = require('lodash');

class Details extends Component {

  constructor(props) {
  super(props);

  }


  componentDidMount() {
    var TIMEOUT = 2000;
    var lastTime = (new Date()).getTime();
    getAllquestions(this.setState.bind(this)); //get list of all currently asked questions
    setInterval(function() {
      var currentTime = (new Date()).getTime();
      if (currentTime > (lastTime + TIMEOUT + 2000)) {
        // Wake!
        window.location = window.location;
      }
      lastTime = currentTime;
    }, TIMEOUT);
  }


  render() {
    return (
        <div className="secondaryPages">
            <div className="container-fluid flex">
            	<div className="g--3 g--sm-12 noPad">
                	<Header currentSpeaker={this.props.currentSpeaker} userlevel={this.props.userlevel} />
				</div>
				<div className="g--9 g--sm-12">
                    <div className={styles.linkList}>
                      <Link to="/agenda">Symposium agenda</Link>
                      <Link to="/questions" className={styles.locked}>Live Poll</Link>
                      <Link to="/askquestion" >Ask Question</Link>
                      
                      <Link to="/speakers">Speaker Biographies</Link>
                      {this.props.userlevel === 'admin' &&
                        <>
                          <Link to="/admin">Admin</Link>
                          <Link to="/addnew0">Add new</Link>
                        </>
                      }
                    </div>
                </div>
			</div>
		</div>
    )
  }
}
export default Details;