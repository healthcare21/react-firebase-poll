import React, { Component } from 'react';
import matt from '../images/profile/profileMatt.png'
import simon from '../images/profile/profileSimon.jpg'
import alex from '../images/profile/profileAlex.jpg'
import { getAllquestions} from '../utilities/firebase_queries'
import Header from './header'
import { Link } from 'react-router-dom';

import styles from './css/speakers.module.scss';

var _ = require('lodash');

class Speakers extends Component {

  constructor(props) {
  super(props);

  }


  componentDidMount() {
    var TIMEOUT = 2000;
    var lastTime = (new Date()).getTime();
    getAllquestions(this.setState.bind(this)); //get list of all currently asked questions
    setInterval(function() {
      var currentTime = (new Date()).getTime();
      if (currentTime > (lastTime + TIMEOUT + 2000)) {
        // Wake!
        window.location = window.location;
      }
      lastTime = currentTime;
    }, TIMEOUT);
  }





  render() {
    return (
            <div className="homepage">
				<div className="container-fluid flex">
					<div className="g--3 g--sm-12 noPad">
						<Header header="Speaker bios" />
					</div>
					<div className="g--9 g--sm-12">
						<div className={styles.speakerList}>

							<div className={styles.speaker}>
								<div className={styles.gradient}></div>
								<div className={styles.image}>
									<div className={styles.imgHolder}>
										<img src={matt} alt="Photo of Matt" />
									</div>
								</div>
								<div className={styles.text}>
									<h2>Matt Burton</h2>
									<p className={styles.role}>Lead Developer, HealthCare21</p>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ex orci, efficitur non varius vitae, ornare lacinia odio. Donec tempus mauris vel tellus ornare</p>
								</div>
							</div>


							<div className={styles.speaker}>
								<div className={styles.gradient}></div>
								<div className={styles.image}>
									<div className={styles.imgHolder}>
										<img src={simon} alt="Photo of Simon" />
									</div>
								</div>
								<div className={styles.text}>
									<h2>Simon Matthews</h2>
									<p className={styles.role}>Digital Lead, HealthCare21</p>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ex orci, efficitur non varius vitae, ornare lacinia odio. Donec tempus mauris vel tellus ornare</p>
								</div>
							</div>

							<div className={styles.speaker}>
								<div className={styles.gradient}></div>
								<div className={styles.image}>
									<div className={styles.imgHolder}>
										<img src={alex} alt="Photo of Alex" />
									</div>
								</div>
								<div className={styles.text}>
									<h2>Alexandra Boocock</h2>
									<p className={styles.role}>Designer, HealthCare21</p>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ex orci, efficitur non varius vitae, ornare lacinia odio. Donec tempus mauris vel tellus ornare</p>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
    )
  }
}
export default Speakers;