import React, { Component } from 'react';
import { faHeart } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class Tertiary extends Component {
  constructor(props) {
    super(props);
    this.state= {
        currentCat : this.props.match.params.id,
        items: [],
        currentItem: {assets: []},
        currentAsset: [],
        header : 'tertiary',
        secondaryCategory: '',
        mainDescription: ''

    }
    this.setAsset = this.setAsset.bind(this);
  }


  componentDidMount() {
    if(this.props.cats.length === 0){
        //No catagories mean the page has been refreshed -- get stored from localstorage
        var lstorage = JSON.parse(localStorage.getItem('tertiaryPage'));
        this.setState({
            items: lstorage,
            secondaryCategory : lstorage[0].data.secondaryCategory,
            mainDescription : lstorage[0].data.mainDescription
        })
        this.props.setheaderText(lstorage[0].data.secondaryCategory);
        
      } else {
        const filterNames = this.props.items.filter(obj => obj.id === this.state.currentCat );
        this.setState({
            items: filterNames,
            secondaryCategory : filterNames[0].data.secondaryCategory,
            mainDescription : filterNames[0].data.mainDescription
        })
        localStorage.setItem('tertiaryPage', JSON.stringify(filterNames));
        this.props.setheaderText(filterNames[0].data.secondaryCategory);
      }
  }

  setAsset(index, resource){
      this.setState({
        currentItem : resource
      }
      )
  }

  setAssetSingle(index, resource){
    this.setState({
        currentAsset : resource,
        secondaryCategory : resource.title,
        mainDescription : resource.description
    }
    )
  }

  addtoMyKameo(id){
    var obj = {'currentId': id, 'title': this.state.items[0].data.secondaryCategory}
    var mykameoBucket = JSON.parse(localStorage.getItem('mykameoBucket'));
    if(!mykameoBucket){ //nothing exists
        localStorage.setItem('mykameoBucket', JSON.stringify([obj]));
    } else {
        //We have an entry check for existing entry
        if(localStorage.getItem('mykameoBucket').includes(id)){
            alert ('This already exists in your My Kameo');
        } else {
            mykameoBucket[mykameoBucket.length] = obj;
            localStorage.setItem('mykameoBucket', JSON.stringify(mykameoBucket));
            alert ('Added to your My Kameo');
        }
    }
  }

  addtoMyKameolist(title,description,type,link){
      var obj = {'title': title, 'description': description, 'type': type, 'link': link}
    var mykameoBucket = JSON.parse(localStorage.getItem('mykameoItem'));
    if(!mykameoBucket){ //nothing exists
        localStorage.setItem('mykameoItem', JSON.stringify([obj]));
    } else {
        //We have an entry check for existing entry
        if(localStorage.getItem('mykameoItem').includes(title) && localStorage.getItem('mykameoItem').includes(link)){
            alert ('This already exists in your My Kameo');
        } else {
            mykameoBucket[mykameoBucket.length] = obj;
            localStorage.setItem('mykameoItem', JSON.stringify(mykameoBucket));
            alert ('Added to your My Kameo');
        }
    }
  }
  

  render() {
      let embed; let button;
      if(this.state.currentAsset.type === 'video'){
          embed = '<iframe width="560" height="315" src="https://www.youtube.com/embed/'+this.state.currentAsset.link+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
          button = <Kameobutton onClick={() => this.addtoMyKameolist(this.state.currentAsset.title, this.state.currentAsset.description,this.state.currentAsset.type,this.state.currentAsset.link  )} />
         } 
      if(this.state.currentAsset.type === 'image'){
        embed = '<img src="'+this.state.currentAsset.link+'" />'
        button = <Kameobutton onClick={() => this.addtoMyKameolist(this.state.currentAsset.title, this.state.currentAsset.description,this.state.currentAsset.type,this.state.currentAsset.link  )} />
     } 
      if(this.state.currentAsset.type === 'url' || this.state.currentAsset.type == 'pdf') {
        embed = '<a href="'+this.state.currentAsset.link+'" target="_blank"/>Click here</a>'
        button = <Kameobutton onClick={() => this.addtoMyKameolist(this.state.currentAsset.title, this.state.currentAsset.description,this.state.currentAsset.type,this.state.currentAsset.link  )} />
      } 
  
  
    
    return (
      <div>
       <span className="headerMyKameo" onClick={() => this.addtoMyKameo(this.state.currentCat)}><FontAwesomeIcon icon={faHeart} /></span>
            <div className="wrapper tertiary">
          
              {this.state.items.map((item) => {
                  return (
                    <div className="tertiaryList">
                        <div className="headerSecondary">
                        {button}
                            <h3>{this.state.secondaryCategory}</h3>
                            <h4>{this.state.mainDescription}</h4>
                            <div dangerouslySetInnerHTML={{ __html: embed }} />
                            {/* <h4>{this.state.currentItem}</h4> */}
                        </div>

                        <div className="resourceIndex">
                                <h3><span className="boldText">{item.data.secondaryCategory}</span><br/>Resource Index</h3>
                                <div className="resourceList">
                                    {item.data.resources.map((resource,index) => {
                                        return (
                                          <div className="resourceLine">
                                            <h5 key={resource.name} onClick={() => this.setAsset(index, resource)} >{resource.name} 
                                         
                                            </h5> 
                                            <div className="resourceCount">
                                               <div>
                                                 <p>{resource.assets.length}</p><p><span className="filesSpan">Files</span></p>
                                                 </div>
                                            </div>
                                            </div>
                                            )
                                    })
                                    }
                                </div>
                        </div>
                  </div>
                  )
              })} 
            
              <div className="assetList">
                    {this.state.currentItem.assets.map((current,index) => {
  
                        return (
                          <div className="video" key={index} >
                            <div className="titles">
                              <h5 onClick={() => this.setAssetSingle(index, current)}>{current.title}</h5>
                              <h6 onClick={() => this.setAssetSingle(index, current)}>{current.subtitle}</h6>
                            </div>
                          </div>
                        )
                    })
                    }
              </div>
            </div>
            </div>
    )
  }
}

function Kameobutton(props) {
  return (
       <span onClick={props.onClick} className="MyKameomain" ><FontAwesomeIcon icon={faHeart} /></span>
  );
}


export default Tertiary;