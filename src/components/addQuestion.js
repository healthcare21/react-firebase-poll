import React, { Component } from 'react';
import firebase, { } from '../firebase.js';

import Header from './header'

import styles from './css/addQuestion.module.scss';

var _ = require('lodash');
class AddQuestion extends Component {

  constructor(props) {
    super(props);
    this.projectRef = firebase.firestore().collection('questions');
    this.state = {
        currentCat : this.props.match.params.id,
        singleItem : '',
        Title: 'title',
        Description: 'description',
        mainCategory: '',
        secondaryLevelboo: false,
        secondaryCategory: '',
        Answer1: '',
        Answer2: '',
        Answer3: '',
        Answer4: '',
        subtitle: '',
        username: '',
        items: [],
        resources: [{name: '',bucketType: '', description: '', assets: [{title: '', link: '', type: ''}]}]
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  
  }

  componentDidMount() {
   //Get list of current questions
  
}

  handleChangeCheckbox(e) {
    this.setState({
        [e.target.name]: e.target.checked
    });
  }
  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }
  


  handleSubmit(e) {
    e.preventDefault();
    if (!this.state.Title) {
        alert ('Please enter a title');
    } else {
        let QuestionsArr = [];
        let answer1 = {'Answer':this.state.Answer1, 'Number': 0};
        let answer2 = {'Answer':this.state.Answer2, 'Number': 0};
        let answer3 = {'Answer':this.state.Answer3, 'Number': 0};
        let answer4 = {'Answer':this.state.Answer4, 'Number': 0};
        QuestionsArr.push(answer1,answer2,answer3, answer4);

        this.projectRef.add({
                Title: this.state.Title,
                Descriptions: this.state.Description,
                questionsArray: QuestionsArr,
          //     secondaryCategory: this.state.secondaryCategory,
          //     resources: this.state.resources
        });  

            // this.projectRef.add({
            //     mainCategory: this.state.mainCategory,
            //     secondaryLevelboo: this.state.secondaryLevelboo,
            //     secondaryCategory: this.state.secondaryCategory,
            //     resources: this.state.resources
            // });  
            // this.setState({
            //   mainCategory: '',
            //   secondaryLevelboo: false,
            //   secondaryCategory: '',
            //   subtitle: '',
            //   username: '',
            //   items: [],
            //   resources: [{name: '',bucketType: '', description: '', assets: [{title: '', link: '', type: ''}]}]
            // });
    }
  }

  removeItem(itemId) {
    this.projectRef.doc(itemId).delete();
  }

  appendInput() {
    var newInput = {name: '',bucketType: '', assets: [{title: '', link: '', type: ''}]}
    this.setState(prevState => ({ resources: prevState.resources.concat([newInput]) }));
  }

  appendInputR(index, indexr) {
     var newInput = {title: '', link: '', type: ''}
     var concat = this.state.resources[index].assets.concat(newInput);
     this.setState(prevState => ({ assets: prevState.resources[index].assets = concat }));
  }

  createList = (questions) => {
    let returnQuestion = [];
    if(questions.question){
      for (var i = 0; i < questions.question.length; i++) {
        returnQuestion.push(<li>{questions.question[i]}</li>);
      }
    }
    return returnQuestion;
  }

  render() {
    let question;
    if(this.props.currentQuestions){
      this.singleItem =   _.filter(this.props.currentQuestions, { 'id': this.props.match.params.id });
       question = _.filter(this.props.currentQuestions, { 'id': this.props.match.params.id });
        if(question[0]){
       question = question[0].data.questionsArray;
        }
      }
    return (
		<div class="addQuestion">
			<div className="container-fluid flex">
				<div className="g--3 g--sm-12 noPad">
          			<Header currentSpeaker={this.props.currentSpeaker} userlevel={this.props.userlevel} />
				</div>
				<div className="g--9 g--sm-12">
				<div className={['card', styles.addQuestion].join(' ')}>
					<h2>Add question </h2>
					{/* {this.state.Title} :  {this.state.Description} */}
					
					<form onSubmit={this.handleSubmit}>
						<input type="text" name='Title' placeholder="Title" onChange={(e) => this.handleChange(e)} value={this.state.Title} />          
						<input type="text" name='Description' placeholder="Description" onChange={(e) => this.handleChange(e)} value={this.state.Description} />              
												
						<input type="text" name='Answer1' placeholder="Answer1" onChange={(e) => this.handleChange(e)} value={this.state.Answer1} />            
						<input type="text" name='Answer2' placeholder="Answer2" onChange={(e) => this.handleChange(e)} value={this.state.Answer2} />                 
						<input type="text" name='Answer3' placeholder="Answer3" onChange={(e) => this.handleChange(e)} value={this.state.Answer3} />               
						<input type="text" name='Answer4' placeholder="Answer4" onChange={(e) => this.handleChange(e)} value={this.state.Answer4} />   

						<button>Submit</button>
					</form>
					
					{/* <h2>Current Answers</h2> */}

					{/* <ul>
					{this.createList({question})}
					</ul> */}

				</div>
				</div>
			</div>
		</div>
    );
  }
}
export default AddQuestion;