import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {  faTrash } from '@fortawesome/free-solid-svg-icons'

class Mykameo extends Component {
  constructor(props) {
    super(props);
    this.state= {
        mykameoBucket: [],
        singleitems: [],
        chosenItem: []
    }
  }


  componentDidMount() {
    //get top level myKameo items form local storage {mykameoBucket}
    if(localStorage.getItem('mykameoBucket')){
      var bucket = JSON.parse(localStorage.getItem('mykameoBucket'));
      this.setState({
        mykameoBucket: bucket,
    })
    }

    if(localStorage.getItem('mykameoItem')){
      var items = JSON.parse(localStorage.getItem('mykameoItem'));
      this.setState({
        singleitems: items
    })
    }
  } 

  removeItem(index){
        var allToplevel =  this.state.mykameoBucket;
        allToplevel.splice(index, 1);
        localStorage.setItem('mykameoBucket', JSON.stringify(allToplevel));
        this.setState({
            mykameoBucket: allToplevel
        })
  }

  removeItemSingle(index){
    var allSingles =  this.state.singleitems;
    allSingles.splice(index, 1);
    localStorage.setItem('mykameoItem', JSON.stringify(allSingles));
    this.setState({
        singleitems: allSingles
    })
  }
  setItem(item){
    this.setState({
        chosenItem: item
    })
  }

  render() {
    let embed; 
    if(this.state.chosenItem.type === 'video'){
        embed = '<iframe width="560" height="315" src="https://www.youtube.com/embed/'+this.state.chosenItem.link+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
       } 
    if(this.state.chosenItem.type === 'image'){
      embed = '<img src="'+this.state.chosenItem.link+'" />'
   } 
    if(this.state.chosenItem.type === 'url' || this.state.chosenItem.type === 'pdf') {
      embed = '<a href="'+this.state.chosenItem.link+'" target="_blank"/>Click here</a>'
  
    } 

    return (
            <div className="homepage mykameo">
            <h1>MyKameo</h1>
            <h4>Top level pages</h4>
            <ul>
              {this.state.mykameoBucket.map((item, index) => {
                  return (
                    <li key={item.title}>
                       <Link to = {'/tertiary' + item.currentId}>
                         <h3>{item.title}</h3>
                         </Link>
                         <button onClick={() => this.removeItem(index)}>Remove <FontAwesomeIcon icon={faTrash} /></button>
                      
                    </li>
                  
                  )
              })} 
              </ul>

             <h4>Single Elements</h4>
            <ul class="singleelements">
              {this.state.singleitems.map((item, index) => {
                  return (
                    <li key={item.title}  onClick={() => this.setItem(item)}>
                          <h5>{item.title}</h5>   
                         <button onClick={() => this.removeItemSingle(index)}>Remove <FontAwesomeIcon icon={faTrash} /></button>
                    </li>
                  )
              })} 
              </ul>

              <h3>Details</h3>
              <h2>{this.state.chosenItem.title}</h2>
                   <p>{this.state.chosenItem.description}</p>
                   <div dangerouslySetInnerHTML={{ __html: embed }} />

            </div>
    )
  }
}
export default Mykameo;