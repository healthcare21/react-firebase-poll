import React, { Component } from 'react';
import {logout} from '../utilities/app'
import { Link } from 'react-router-dom';

import iconHome from '../images/icons/iconHome.png'
import iconLogout from '../images/icons/iconLogout.png'

import styles from './css/header.module.scss'

class Header extends Component {
    constructor(props) {
        super(props);
        }
    render() {

        let speakerName, preSpeaker;
        let theSpeaker;
        if (this.props.currentSpeaker !== 'Null' && this.props.currentSpeaker ){
            theSpeaker= (<div className={styles.currentSpeaker}><p><strong>Current speaker: </strong>{this.props.currentSpeaker}</p></div>)
        }
        let header = ( this.props.header ? this.props.header : 'Poll Tracker' );

        let links = (
            <>
                <Link to="/agenda">Symposium agenda</Link>
                <Link to="/questions" className={styles.locked}>Live Poll</Link>
                <Link to="/askquestion">Ask Question</Link>
                <Link to="/speakers">Speaker Biographies</Link>
            </>
        );
        if( this.props.userlevel === 'admin' ){
            links = (
                <>
                    <Link to="/admin">Admin</Link>
                    <Link to="/addnew0" className={styles.add}>Add a question</Link>
                    <Link to="/presentation">View presentation</Link>
                </>
            );
        }



        // let secondary = 'HC21 - Event polling'
        return (
            <header>
                <div class="container">
                    <div class="g--12">
                        <h1>{header}</h1>
                        {/* <h3>{secondary}</h3> */}
                        <p className={styles.logoutIcon} onClick={ () => logout(this.setState.bind(this))}><img src={iconLogout} alt="Logout" /></p>
                        <Link to="/" className={styles.homeIcon}><img src={iconHome} alt="Home" /></Link>
                        {theSpeaker}
                        <nav className={styles.desktopNav}>
                            {links}
                            <p className={[styles.button].join(' ')} onClick={ () => logout(this.setState.bind(this))}>Logout</p>
                        </nav>
                    </div>
                </div>
            </header>
        )
    }
}


export default Header;  