import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import firebase  from '../firebase.js';
import { getAllquestions} from '../utilities/firebase_queries'
import Header from './header'

import styles from './css/askQuestion.module.scss'

var _ = require('lodash');

class askQuestions extends Component {

  constructor(props) {
  super(props);
   this.state = {
      currentItem: '',
      username: '',
      items: [],
      user: null,
      locked: false,
      currentQuestion: '',
      barGraphdata: [],
      question: '',
      allQuestions: []
    }
    this.questionRef = firebase.firestore().collection('questions');
    this.userQuestions = firebase.firestore().collection('Questions');
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    // this.acceptQuestion = this.acceptQuestion.bind(this);
    // this.createQuestionsMod = this.createQuestionsMod.bind(this);
  }


  componentDidMount() {
    var TIMEOUT = 2000;
    var lastTime = (new Date()).getTime();
    getAllquestions(this.setState.bind(this)); //get list of all currently asked questions
    setInterval(function() {
      var currentTime = (new Date()).getTime();
      if (currentTime > (lastTime + TIMEOUT + 2000)) {
        // Wake!
        window.location = window.location;
      }
      lastTime = currentTime;
    }, TIMEOUT);
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  handleSubmit(e) {
    e.preventDefault();
    //add to userQuestions
    this.userQuestions.add({
      Text: this.state.question,
      moderated: false,
      pollID: this.props.currentState
    })
    alert('Question posed');
  }

  getQuestion(questionId) {
    let newarr;
    if(this.props.currentState !== 'notStarter') {
      newarr =   _.filter(this.props.currentQuestions, { 'id': this.props.currentState });
    }
    this.state.currentItem = newarr;
    return newarr;
  }

  onItemClick = (item) => {
    let temporaryarray = this.state.currentItem[0].data;
    temporaryarray.questionsArray[item].Number = temporaryarray.questionsArray[item].Number +1;
    this.setState({
      locked:true
    })
    //Find item in database and add 1
     this.questionRef.doc(this.state.currentItem[0].id).update({
      questionsArray: temporaryarray.questionsArray
     })
     this.setState({
       locked: true
     })
  }




  createList = (questions) => {
    let content;
    if(questions.questions){
      // if( this.state.locked ){ //Cannot use state you dimwitted FOOL!
      //    content = questions.questions.map((catName, i) => {
      //     return (
      //       <li
      //         key ={i}
      //       >
      //       <p>{catName.Answer}</p>
      //       <p>{catName.Number}</p>
      //       </li>
      //     )
      //   });

      // } else {
        content = questions.questions.map((catName, i) => {
        return (
			<div className={styles.answerCard}>
				<p>{catName.Answer}</p>
				<button onClick = {() => {this.onItemClick(i)}}>VOTE</button>
				{/* <div className="liSubstitute"
				key ={i}
				onClick = {() => {this.onItemClick(i)}}
				>
				
				<p>Submit</p>
				</div> */}
			</div>
        )
      });
    // }
   return content;
  }
}




  render() {
	let title; let description; let questions; let question;

	let afterText;
	let backButton;
	let closed = false;

     if(this.props.allowQuestion ){
        question = <div className={styles.postQuestion}><p>Ask a question about this poll (questions are moderated)</p>
        <form onSubmit={this.handleSubmit} className="questionForm">                 
             <textarea name="question" form="usrform" onChange={(e) => this.handleChange(e)}>{this.state.question}</textarea>
           <button>Submit</button>
       </form></div>
     } 
    // if(!this.props.currentState || this.props.currentState === 'notStarted'){   //display welcome message - not started!
    //   title = "The poll has not been started yet, please wait - this screen will automatically refresh when the poll is open";
	  // description = "Instructions for the poll"
	  // afterText = 'Polling is currently closed. Check back again later.'
	  // backButton = (<Link to="/" className={styles.backButton}>Back</Link>)
	  // closed = true
    // } else {
     
    //   let currentQ = this.getQuestion();  //get question details from props
    //   title = currentQ.data;
    //   if(currentQ[0]) {
    //     title = currentQ[0].data.Title;
    //     description = currentQ[0].data.Descriptions;
    //     questions = currentQ[0].data.questionsArray;
    //   }
    // }
   
    return (
            <div className="homepage">
				<div className="container-fluid flex">
					<div className="g--3 g--sm-12 noPad">
						<Header header="Ask a question" />
					</div>
					<div className="g--9 g--sm-12">
						<div className={['card', styles.questionContainer, styles.noPad].join(' ')}>
							<div className={styles.theQuestion}>
								{this.createList({questions})}
								{question}
							</div>
							{backButton}
						</div>
						<p className={styles.afterText}>{afterText}</p>
					</div>
				</div>
				{/* <h1 className="hundredWidth">{title}</h1>
				<h2>{description}</h2> */}
				{/* {this.state.locked} { 
					<h4 className="warning">Thank you for your submission</h4>
				} */}
            </div>
    )
  }
}
export default askQuestions;