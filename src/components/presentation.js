import React, { Component } from 'react';
import firebase from '../firebase.js';
import { BarChart, Tooltip, Legend, Bar, CartesianGrid, XAxis, YAxis } from 'recharts';
import { getAllquestions} from '../utilities/firebase_queries'
import {questionsPerPoll} from '../globals.js'; 

import styles from './css/presentation.module.scss'

var _ = require('lodash');

class Presentation extends Component {

  constructor(props) {
  super(props);
   this.state = {
      currentItem: '',
      username: '',
      items: [],
      user: null,
      locked: false,
      currentQuestion: '',
      barGraphdata: []
    }
    this.questionRef = firebase.firestore().collection('questions');
  }


  componentDidMount() {
    getAllquestions(this.setState.bind(this)); //get list of all currently asked questions
   
  }

  getQuestion(questionId) {
    let newarr;
    if(this.props.currentState !== 'notStarter') {
        //filterquestions by id
      newarr =   _.filter(this.props.currentQuestions, { 'id': this.props.currentState });
    }
    return newarr;
  }

  onItemClick = (item) => {
    let temporaryarray = this.state.currentItem[0].data;
    temporaryarray.questionsArray[item].Number = temporaryarray.questionsArray[item].Number +1;
    this.setState({
      locked: true
    })
    //Find item in database and add 1
     this.questionRef.doc(this.state.currentItem[0].id).update({
      questionsArray: temporaryarray.questionsArray
     })
     this.setState({
      locked : true
    })
  }


  greeting = () => {
    if(this.state.locked){
      return(
      	<h3 className="warning">Thank you for your submission</h3>
      )
    }
  }

  createList = (questions) => {
    let content;
    if(questions.questions){
        content = questions.questions.map((catName, i) => {
              return (
                <div className={styles.answer} key={i}>
                <p><span className="totalNumber">{catName.Number}</span>{catName.Answer}</p>
                </div>
              )
            });
          }
     return content;
  }

  userQuestions = (questions, currentID) => {
    let content;
    if(questions.userQuestions){
        content = questions.userQuestions.map((catName, i) => {
                if(catName.IsModerated){
                  if(questionsPerPoll){
                    if(catName.pollId === currentID.currentId){
                      return (
                        <div className="fiftyadmin" key={i}>
                        <p><span className="spanQuestion">Q:{currentID.currentID}</span> {catName.Title}</p>
                        </div>
                      )
                    }
                } else{ 
                  return (
                    <div className="fiftyadmin" key={i}>
                    <p><span className="spanQuestion">Q:{currentID.currentID}</span> {catName.Title}</p>
                    </div>
                  )
                }
              }

            });
          }
     return content;
  }
  


buildChart(questions){
    if(questions.questions){
      let barGraphdataTemp = [];
      questions.questions.map((question, i) => {
        let tempObj = {name: question.Answer, votes: question.Number};
        barGraphdataTemp.push(tempObj);
      })
      let renderLineChart = (
        <BarChart
          width={800}
          height={400}
          data={barGraphdataTemp}
          margin={{
            top: 5, right: 30, left: 20, bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Bar dataKey="votes" fill="#82ca9d" />
        </BarChart>
      );
      return renderLineChart
    }

}



  render() {
    if(this.props.userlevel){
      if(this.props.userlevel != 'admin'){
        window.location.href = './';
      }
    }
    let title; let description; let questions; let currentId;
    let userQuestions = this.state.questions;
    if(!this.props.currentState || this.props.currentState === 'notStarted'){
      //display welcome message - not started!
      title = "The poll has not been started yet, please wait - this screen will automatically refresh when the poll is open";
      description = "Instructions for the poll"
      currentId = '';
    } else {
      //get question details from props
      let currentQ = this.getQuestion();
      title = currentQ.data;
      if(currentQ[0]) {
        title = currentQ[0].data.Title;
        description = currentQ[0].data.Descriptions;
        questions = currentQ[0].data.questionsArray;
        currentId = currentQ[0].id;
      }
    }
   
    return (
            <div className={styles.presentation}>
				<div className="container-fluid">
					<div className={['card', 'flex', styles.fullPage].join(' ')}>
						<div class="g--7">
							<div className={styles.question}>
								<h1>{title}</h1>
								<h2>{description}</h2>
							</div>
							<div className="barchart">
								{this.greeting()} 
								{this.buildChart({questions})}
							</div>
							<div className={styles.results}>
								<div className={['flex', styles.answerList].join(' ')}>
									{this.createList({questions})}
								</div>
							</div>
						</div>
						<div class="g--4 offset--1">
							<div className={styles.sidebar}>
								<div className={styles.moderatedQuestions}>
									<h3>Latest audience questions</h3>
									<div className={styles.questionList}>
										<div className={styles.question}>
											<p>This is just a test question to determine the output of the question list on the presentation page, this is hardcoded.</p>
											{/* <p><span className="spanQuestion">Q:{currentID.currentID}</span> {catName.Title}</p> */}
										</div>
										<div className={styles.question}>
											<p>This is just a test question to determine the output of the question list on the presentation page, this is hardcoded.</p>
											{/* <p><span className="spanQuestion">Q:{currentID.currentID}</span> {catName.Title}</p> */}
										</div>
										<div className={styles.question}>
											<p>This is just a test question to determine the output of the question list on the presentation page, this is hardcoded.</p>
											{/* <p><span className="spanQuestion">Q:{currentID.currentID}</span> {catName.Title}</p> */}
										</div>
										<div className={styles.question}>
											<p>This is just a test question to determine the output of the question list on the presentation page, this is hardcoded.</p>
											{/* <p><span className="spanQuestion">Q:{currentID.currentID}</span> {catName.Title}</p> */}
										</div>
										<div className={styles.question}>
											<p>This is just a test question to determine the output of the question list on the presentation page, this is hardcoded.</p>
											{/* <p><span className="spanQuestion">Q:{currentID.currentID}</span> {catName.Title}</p> */}
										</div>
										{this.userQuestions({userQuestions},{currentId})}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
         
    )
  }
}
export default Presentation;