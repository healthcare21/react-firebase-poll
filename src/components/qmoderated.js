import React, { Component } from 'react';
import firebase, { auth } from '../firebase.js';

import styles from './css/qmoderated.module.scss'

class DangerButton extends Component {
  isModerated(moderated) {
    if(moderated){
      return <span className="green">Moderated</span>;
    } else {
      return <span className="red">Not moderated</span>;
    }
  }

  setModerated(itemId, action){
    if(action === 'accept'){
      action = true;
    } else {
      action = false;
    }
    //change data in doc.
    //Move this to firebase.js - when happy
    let questionsRef = firebase.firestore().collection('Questions'); 
    questionsRef.doc(itemId).update({moderated: action});

  }

  moderatedLink(moderated, modId, setModerated) {
    if(moderated){
      return <p className="red" onClick = {() => {setModerated(modId, 'revoke')}}>Revoke moderated</p>;
    } else {
      return <p className="green" onClick = {() => {setModerated(modId, 'accept')}}>Accept Moderation</p>;
    }
  }

  filteQuestions = (questions) => {
    let filtertedItems = []; let curId = this.props.currentID; let ismoderated =this.isModerated; let moderatedLink = this.moderatedLink;
    let setModerated = this.setModerated;
    {questions.map((function(question, i) {
		if(question.pollId === curId ) {
		//    filtertedItems.push(<div key={i} >{question.Title} : Is moderated {ismoderated(question.IsModerated)}  {moderatedLink(question.IsModerated, question.id, setModerated)} </div>)
			filtertedItems.push(
				<div className={['flex', styles.moderationContainer].join(' ')} key={i} onClick={() => setModerated(question.id, (question.IsModerated ? 'revoke' : 'accept'))}>
					<p>Moderate:</p>
					<div className={[styles.moderationToggle, (question.IsModerated ? styles.moderationOn : styles.moderationOff)].join(' ')}>
						<span className={styles.toggleBar}></span>
					</div>
					{/* {ismoderated(question.IsModerated)}
					{moderatedLink(question.IsModerated, question.id, setModerated)} */}
				</div>
			)
		}
    }
    ))}
    return filtertedItems
  }

  render() {
    return (
		<>
			{this.props.questions ? (
				<>
					{/* <h3>Current user questions</h3> */}
					{this.filteQuestions(this.props.questions)}
				</>
				)
			: 
				null
			}
		</>
    )
  }
}

export default DangerButton;  

