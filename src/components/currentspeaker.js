import React, { Component } from 'react';
import firebase, { auth } from '../firebase.js';

import styles from './css/currentspeaker.module.scss'


class CurSpeaker extends Component {

  constructor(props) {
    super(props);
  
    }
    setSpeaker(spearkername) {
      let currentSpeaker = firebase.firestore().collection('currentSpeaker'); 
      currentSpeaker.doc('UH7dwuTJMbUAnxZWuHmU').update({Name: spearkername});
    }


  filteSpeakers = (speakers) => {
    let currentSpeaker = this.props.currentSpeaker; let setSpeaker = this.setSpeaker;
    let filtertedItems = []; let curId = this.props.currentID; 
    {speakers.map((function(speaker, i) {
      if(speaker.Name === currentSpeaker){
        filtertedItems.push(<div key={i} ><p className={['button', styles.speakerName, styles.lessPad, styles.currentSpeaker].join(' ')}>{speaker.Name}</p> </div>)
      } else {
       filtertedItems.push(<div key={i} ><p className={['button', styles.speakerName, styles.lessPad].join(' ')} onClick={() => {setSpeaker(speaker.Name)}}>{speaker.Name}</p></div>)
      }
    }
    ))}
    return filtertedItems
  }

  render() {
    return (
		<>
			{this.props.speakers ? (
				<>
					<h3 className="questionsTitle">Choose speaker</h3>
					{this.filteSpeakers(this.props.speakers)}
				</>
				)
			: 
				null
			}
		</> 
    )
  }
}

export default CurSpeaker;  