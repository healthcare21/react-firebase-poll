import React, { Component } from 'react'

import './css/footer.module.scss'

import hc21logo from '../images/logos/logoHC21.png'

class Footer extends Component {
    render() {
        return (
            <footer>
                <div className="container">
                    <div class="g--12">
                        <img src={hc21logo} alt="Built by HealthCare21" />
                    </div>
                </div>
            </footer>
        )
    }
}
export default Footer