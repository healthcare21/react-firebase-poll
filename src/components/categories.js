import React, { Component } from 'react';
import firebase, {} from '../firebase.js';

class Catagories extends Component {

  constructor(props) {
    super(props);
    this.projectRef = firebase.firestore().collection('catagories');
    this.state = {
        mainCategory: '',
        subtitle: '',
        items: []
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }


  handleSubmit(e) {
    e.preventDefault();
    if (!this.state.mainCategory) {
        alert ('Please enter a title');
    } else {
            this.projectRef.add({
                title: this.state.mainCategory,
                subtitle: this.state.subtitle
            });  
            this.setState({
                mainCategory: '',
                subtitle: ''
            });
    }
  }

  removeItem(itemId) {
    this.projectRef.doc(itemId).delete();
  }

  render() {
  
    return (

        <div>
            <h2>Add top level catagory</h2>
             <form onSubmit={this.handleSubmit}>
                {/* <input readOnly type="text" name="username" placeholder="What's your name?" value={this.state.user.displayName || this.state.user.email} />
                <input type="text" name="currentItem" placeholder="What are you bringing?" onChange={this.handleChange} value={this.state.currentItem} /> */}
                <input readOnly type="hidden" name="username" placeholder="What's your name?" value={this.props.user.displayName || this.props.user.email} />   
                <input type="text" name="mainCategory" placeholder="Title" onChange={this.handleChange} value={this.state.mainCategory} /> 
                <input type="text" name="subtitle" placeholder="Sub title" onChange={this.handleChange} value={this.state.subtitle} /> 
                <button>Add Catagory</button>
            </form>
            <h2>Current top level catagories</h2>
            {this.props.cats.map((item) => {
                     return (
                           <p key = {item.id}> {item.data.title} <button onClick={() => this.removeItem(item.id)}>X</button> </p>
                     )
             })
             }

        </div>
    );
  }
}
export default Catagories;