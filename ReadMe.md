# React Firebase
## Build this example from the repo.
Run :
`npm install`

Start React using:
`npm start`

## Basic CRUD without Authorisation (do not use for production)

Setup React with react CLI
```
`npm install -g create-react-app`
`    `
`create-react-app appName`
`cd appName`
`yarn add firebase —dev`
`yarn start`
```

Create a firebase file - firebase.js - this will hold our database credentials - example database:

```
import firebase from ‘firebase’
const config = {
    apiKey: “AIzaSyAPUHtRXtL-I0NPkWmhwhUb8NCRqY9y0uQ”,
    authDomain: “kameo-b69cc.firebaseapp.com”,
    databaseURL: “https://kameo-b69cc.firebaseio.com”,
    projectId: “kameo-b69cc”,
    storageBucket: “kameo-b69cc.appspot.com”,
    messagingSenderId: “1027930348024”
  };
firebase.initializeApp(config);
export default firebase;
```

## Creating data
Bind handleSubmit to function by adding 
`this.handleSubmit = this.handleSubmit.bind(this);`
To the constructor()

The function that sets the data is:

```
  handleSubmit(e) {
    e.preventDefault(); //Stops the page from refreshing
    this.projectRef.add({
        title: this.state.currentItem,
        user: this.state.username
      });  
  }
```



## Reading data with onSnapshot
**Import dependancies**

```
import React, { Component } from ‘react’;
import firebase from ‘./firebase.js’; 
```

This gets the database and its credentials.

Add a reference to the database in the constructor as per:
`    this.projectRef = firebase.firestore().collection(‘project’);`

On component mount we will need to start the socket connection to immediately populate our data.

```
 componentDidMount() {
    this.unsubscribeCol = this.projectRef.onSnapshot(this.onColUpdate);
  }
```

This subscribes to the service and calls the function onColUpdate (displayed below), we will use _this.unsubscribeCol_  later to unsubscribe from the service in componentWillUnmount()  function.

```
  onColUpdate = (snapshot) => {
    const docs = snapshot.docs.map((docSnapshot) => ({
      id: docSnapshot.id,
      data: docSnapshot.data()
    }));
  
    this.setState({
      items: docs
    });
  };
```

This sets our state variable items as the results of the database call.
This will also update the data when or if the data changes without any further programming.

## Displaying the state
We use .map to loop around the items state array:
```
   <ul>
        {this.state.items.map((item) => {
                return (
                  <li key={item.id}>
                    <h3>{item.data.title}</h3>
                    <p>brought by: {item.data.user}</p>
                    <button onClick={() => this.removeItem(item.id)}>Remove Item</button>
                    <button onClick={() => this.setItem(item.id)}>Change to Matt</button>
                  </li>
                )
             })} 
          </ul>
```


## Deleting an item
The code above has a reference to a function onClick - as per:
` <button onClick={() => this.removeItem(item.id)}>Remove Item</button>`

This passes the items id  to the function, the function gets the document from the previously defined collection (this.projectRef) and deletes the item with the following code.

```
 removeItem(itemId) {
    this.projectRef.doc(itemId).delete();
  }
```


## Updating the item
The display loop also has a button that calls an update function. For this example I’m just setting the user to an arbitrary field name - ‘Matt B’ - obviously this would in normal circumstances be a form value that would contain the complete data to update or a flag etc. 

```
	setItem(itemId) {
    let projectReft = this.projectRef; //set this to a variable so we can access it in the promise
     this.projectRef.doc(itemId).get().then(function(doc) {
      if (doc.exists) {
        let projectData = doc.data();
        projectData.user = ‘Matt B’;
        projectReft.doc(itemId).set(projectData)
       } else {
        console.log(“No such document!”);
        }
     }).catch(function(error) {
      console.log(“Error getting document:”, error);
    });
  }
```

The function gets the document from the collection then sets the user field to ‘Matt B’ The update function is the set command:

`projectReft.doc(itemId).set(projectData)` 
This sets the  new array to the collection in the database. 

::Using set will create a new record if it doesn’t exist::






